import React, { useState, useEffect } from 'react'
import './countdown.css'
import Qa from '../qa/countdown.json'
const Additions = () => {
    const numpadValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, '-', 0, 'X'];

    const [userInput, setUserInput] = useState(0);

    const [isActive, setStart] = useState(false);

    const [renderQno, setQuestionNo] = useState(0);

    const [countDownDisplay, setCountDownDisplay] = useState();

    const [question, setQuestion] = useState();

    const [questionText, setQuestionText] = useState();

    const [score, setScore] = useState(0);

    const usedQuestionList = [];

    const [itqa, setIntervalRef] = useState(111);

    const [isClear, setClear] = useState(false);

    let questionNo = 0;

    const handleAnswer = (e) => {
        if (!isActive) {
            return
        } else {
            const value = e.target.value;
            setUserInput(value);
        }

    }

    const handleNumpad = (number) => {
        if (!isActive) {
            return
        }
        let strInput = userInput + '';

        if (number === "X") {
            setUserInput('');
        } else if (number === "-") {
            strInput = strInput.slice(0, strInput.length - 1);
            setUserInput(strInput);
        }
        else if ((strInput[0] === '0' || strInput[0] === 0)) {
            strInput = strInput.slice(1, strInput.length);
            setUserInput(strInput + '' + number);
        }
        else {
            strInput = userInput + '' + number;
            setUserInput(userInput + '' + number);
        }

        if (question !== undefined) {
            if (parseInt(strInput) === question.a) {
                strInput = '';
                setUserInput('');
                setScore(prev => prev + 1)
                setClear(true);
            } else {
                setClear(false);
            }
        }

    }


    useEffect(() => {
        let countDown = 3;
        if (isActive) {
            setQuestionNo(questionNo)
            setCountDownDisplay(3);
            setIntervalRef(
                setInterval(() => {
                    if (isClear) {
                    } else {
                        countDown -= 1;
                        setCountDownDisplay(countDown);
                        if (countDown < 0) {
                            setClear(false);
                            let randomNewNumber = true;
                            let checker = 0;
                            countDown = 3;
                            setCountDownDisplay(countDown);
                            questionNo++;
                            setQuestionNo(questionNo)
                            let questionRandom = 0;
                            while (randomNewNumber) {
                                checker++;
                                questionRandom = Math.floor(Math.random() * (100) + 0);

                                if (usedQuestionList.indexOf(questionRandom) === -1) {
                                    usedQuestionList.push(questionRandom)
                                    setQuestionText(Qa.level[1].questions[questionRandom].q);
                                    setQuestion(Qa.level[1].questions[questionRandom]);
                                    randomNewNumber = false;
                                }

                                if (checker >= 200) {
                                    setQuestionText(Qa.level[1].questions[questionRandom].q);
                                    setQuestion(Qa.level[1].questions[questionRandom]);
                                    randomNewNumber = true;
                                }
                            }

                        }
                        if (questionNo === 20) {
                            setStart(false)
                            clearInterval(itqa);
                        }
                    }

                }, 1000)
            );
        }
        return;
    }, [isActive, setQuestionNo, questionNo])

    return (
        <div className="countdown__container" style={{ padding: "1rem 0" }}>
            <div className="countdown__topbar">
                <div className={`countdown__left__status`}>
                    <span className={`${renderQno}`}>{renderQno}/20</span>
                </div>

                <div className={`countdown__score`}>
                    <span className={`${renderQno}`}>{score}/20</span>
                </div>

                <div className={`countdown__right__button`} onClick={() => !isActive ? setStart(!isActive) : ''}>
                    GO
                </div>
            </div>
            <div className={`countdown__quesion__container countdown-display`}>
                {countDownDisplay}
            </div>
            <div className={`countdown__quesion__container`}>
                {
                    isActive ? (isClear ? "Correct!" : questionText)
                        : ""
                }


            </div>
            <div className={`countdown__answer__container`}>
                <input className={`countdown__answer__input`} type="number" onChange={handleAnswer} value={userInput} />
            </div>

            <div className={`countdown__numpad__container`}>
                {
                    numpadValues.map((value, i) => {
                        return (
                            <div key={`numpad-${i}`} className={`numpad`} onClick={() => handleNumpad(value)}>
                                {value}
                            </div>
                        )
                    })
                }
            </div>
        </div >
    )
}

export default Additions
