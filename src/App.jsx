import { useState } from 'react'
import './App.css'
import { Link } from "react-router-dom"

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App gradient-bg">
      <h1>Quick Math</h1>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem",
        }}
      >
        <Link to="/additions">Additions</Link> |{" "}
        <Link to="/subtractions">Subtractions</Link>
      </nav>
    </div>
  )
}

export default App
