import React from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom"
import { Additions, Subtractions } from './index.js';
import './index.css'
import App from './App'

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Additions />}  />
      <Route path="additions" element={<Additions />} />
      <Route path="subtractions" element={<Subtractions />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
)
